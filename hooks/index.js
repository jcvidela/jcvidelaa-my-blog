import React from "react";
import AppContext from "../context/index";

export function useAnswers() {
 const {
   state: { answers }
 } = React.useContext(AppContext);

 return answers;
}

export function useNetworks() {
 const {
   state: { networks }
 } = React.useContext(AppContext);

 return networks;
}