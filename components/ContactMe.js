import Link from "next/link";
import { useNetworks } from "../hooks/index.js";
import styles from './cssmodules/Contactme.module.css';
import { faPhoneAlt, faEnvelope, } from "@fortawesome/free-solid-svg-icons";
import { faLinkedinIn, faGithubSquare, } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function ContactCard() {
  const contacts = useNetworks();
  const icons = [
    { title: "LinkedIn", iconName: faLinkedinIn },
    { title: "Github", iconName: faGithubSquare },
    { title: "Phone", iconName: faPhoneAlt },
    { title: "Email", iconName: faEnvelope },
  ];
  return (
    <>
      <h2>Contact me</h2>
      <div className={styles.container}>
        <ul className={styles.list}>
          {contacts.map((_contact) => (
            <li key={_contact.id}>
              <Link href={_contact.url} passHref>
                <a
                  rel="noopener noreferrer"
                  target="_blank"
                  style={{
                    justifyContent: "flex-start",
                    textDecoration: "none",
                    color: "cornflowerblue",
                  }}
                >
                  <i
                    style={{
                      display: "flex",
                      justifyContent: "flex-start",
                      alignItems: "center",
                    }}
                  >
                    <FontAwesomeIcon
                      icon={
                        icons.find((icon) => icon.title === _contact.title)
                          .iconName
                      }
                      className={styles.icon}
                    />
                    <p
                      style={{
                        margin: 0,
                        padding: 0,
                        textDecoration: "none",
                        lineHeight: "1rem",
                      }}
                    >
                      {_contact.user}
                    </p>
                  </i>
                </a>
              </Link>
            </li>
          ))}
        </ul>
      </div>
    </>
  );
}
